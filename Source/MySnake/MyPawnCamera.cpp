// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPawnCamera.h"
#include "../../../../../../Engine/Source/Runtime/Engine/Classes/Components/BoxComponent.h"
#include "../../../../../../Engine/Source/Runtime/Engine/Classes/GameFramework/SpringArmComponent.h"
#include "../../../../../../Engine/Source/Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "MySnakeActor.h"
#include "FoodActor.h"

// Sets default values
AMyPawnCamera::AMyPawnCamera()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<UBoxComponent>("RootModel");
	RootComponent = MyRootComponent;

	CameraSpring = CreateDefaultSubobject<USpringArmComponent>("Spring");
	CameraSpring->SetRelativeLocation(FVector(0, 0, 0));
	CameraSpring->AttachTo(MyRootComponent);

	MyCamera = CreateDefaultSubobject<UCameraComponent>("Camera");
	MyCamera->AttachTo(CameraSpring, USpringArmComponent::SocketName);

	CameraSpring->SetRelativeRotation(FRotator(-90, 0, 0));
	CameraSpring->TargetArmLength = 1300;
	CameraSpring->bDoCollisionTest = false;

	AutoPossessPlayer = EAutoReceiveInput::Player0;
	GameMode = EMyGameMode::STOP;
}

// Called when the game starts or when spawned
void AMyPawnCamera::BeginPlay()
{
	Super::BeginPlay();

	CreateFood();
}

// Called every frame
void AMyPawnCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GameMode == EMyGameMode::RUN) {
			CreateFood();
	}
}

// Called to bind functionality to input
void AMyPawnCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &AMyPawnCamera::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horisontal", this, &AMyPawnCamera::HandlePlayerHorisontalInput);
	PlayerInputComponent->BindAxis("Modes", this, &AMyPawnCamera::HandlePlayerModeInput);
	//PlayerInputComponent->BindAxis("KeyMapMove", this, &AMyPawnCamera::FMove);
}

void AMyPawnCamera::CreateSnakePlayer()
{
	FVector StartLocation = GetActorLocation();
	FRotator StartRotation = GetActorRotation();

	if (GetWorld()) {
		MySnakePlayer = GetWorld()->SpawnActor<AMySnakeActor>(StartLocation, StartRotation);
		GameMode = EMyGameMode::RUN;
		MySnakePlayer->MyPawn = this;
	}
}

void AMyPawnCamera::HandlePlayerVerticalInput(float value)
{
	if (IsValid(MySnakePlayer)) {
		if (value > 0 && MySnakePlayer->LastMoveDirection != EMovementDirection::DOWN) {
			MySnakePlayer->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && MySnakePlayer->LastMoveDirection != EMovementDirection::UP) {
			MySnakePlayer->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void AMyPawnCamera::HandlePlayerHorisontalInput(float value)
{
	if (IsValid(MySnakePlayer)) {
		if (value > 0 && MySnakePlayer->LastMoveDirection != EMovementDirection::LEFT) {
			MySnakePlayer->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && MySnakePlayer->LastMoveDirection != EMovementDirection::RIGHT) {
			MySnakePlayer->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}

void AMyPawnCamera::HandlePlayerModeInput(float value)
{
	if (IsValid(MySnakePlayer)) {
		if (value > 0) {
			if (GameMode == EMyGameMode::RUN) {
				GameMode = EMyGameMode::PAUSE;
			}
			else {
				GameMode = EMyGameMode::RUN;
			}
		}
	}
}

void AMyPawnCamera::CreateFood()
{
	float SpawnX = FMath::FRandRange(-FieldSize, FieldSize);
	float SpawnY = FMath::FRandRange(-FieldSize, FieldSize);
	FVector StartPoint(SpawnX, SpawnY, SpawnZ);

	if (MySnakePlayer) {
		UWorld* MyWorld = GetWorld();
		
		if (MyWorld) {
			if (FoodCount == 0) {
				MyWorld->SpawnActor<AFoodActor>(StartPoint, FRotator(0, 0, 0));
				IncreaseFood();
			}
		}
	}
}
	

int32  AMyPawnCamera::GetScore()
{
	if (MySnakePlayer) {
		return MySnakePlayer->Score;
	}
	return 0;
}

void AMyPawnCamera::SnakeDestroy()
{
	GameMode = EMyGameMode::STOP;
	if (MySnakePlayer)
		MySnakePlayer->Destroy(true, true);
}


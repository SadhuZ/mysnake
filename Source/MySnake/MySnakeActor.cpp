// Fill out your copyright notice in the Description page of Project Settings.


#include "MySnakeActor.h"
#include "../../../../../../Engine/Source/Runtime/Engine/Classes/Components/SphereComponent.h"
#include "../../../../../../Engine/Source/Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "../../../../../../Engine/Source/Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "../../../../../../Engine/Source/Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "MyPawnCamera.h"

// Sets default values
AMySnakeActor::AMySnakeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<USphereComponent>("SnakeRoot");
	RootComponent = MyRootComponent;
	MyRootComponent->SetRelativeLocation(GetActorLocation());

	CreateSnakeBody();
	//MoveDirection = EMovementDirection::DOWN;
	MovementTimeInterval = 0.5f;
}

// Called when the game starts or when spawned
void AMySnakeActor::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementTimeInterval);
}

// Called every frame
void AMySnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetVisibleBodyElements();
	if (MyPawn->GetMyGameMode() == EMyGameMode::RUN) {
		MoveSnake();
	}
	CollideTail();
}

void AMySnakeActor::CreateSnakeBody()
{
	class UStaticMesh* SnakeElementMesh;
	SnakeElementMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;

	class UMaterialInstance* BodyMaterial;
	BodyMaterial = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(
		TEXT("/Game/SnakeContent/Materials/InstanceMaterials/M_Tech_Hex_Tile_Inst.M_Tech_Hex_Tile_Inst")).Get();
	class UMaterialInstance* HeadMaterial;
	HeadMaterial = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(
		TEXT("/Game/SnakeContent/Materials/InstanceMaterials/M_Tech_Hex_Tile_Pulse_Inst.M_Tech_Hex_Tile_Pulse_Inst")).Get();

	FVector NextPoint = GetActorLocation();
	FName BodyElementName;
	FString Str;
	for (int32 i = 0; i < SnakeSize; ++i) {
		Str = "BodyElement" + FString::FromInt(i);
		BodyElementName = FName(*Str);
		class UStaticMeshComponent* BodyElement;
		BodyElement = CreateDefaultSubobject<UStaticMeshComponent>(BodyElementName);
		BodyElement->SetStaticMesh(SnakeElementMesh);
		BodyElement->SetRelativeLocation(NextPoint);
		BodyElement->AttachTo(MyRootComponent);
		if (i == 0) {
			BodyElement->SetMaterial(0, HeadMaterial);
		}
		else {
			BodyElement->SetMaterial(0, BodyMaterial);
			BodyElement->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}

		SnakeBody.Add(BodyElement);
		NextPoint.X -= SnakeStep;
	}
}

void AMySnakeActor::SetVisibleBodyElements()
{
	for (int32 i = 0; i < SnakeBody.Num(); ++i) {
		if (i < VisibleBodyElements) {
			SnakeBody[i]->SetVisibility(true);
		}
		else {
			SnakeBody[i]->SetVisibility(false);
		}
	}
}

void AMySnakeActor::MoveSnake()
{	
	for (int32 BodyElement = SnakeBody.Num() - 1; BodyElement > 0; --BodyElement) {
		FVector Location =  SnakeBody[BodyElement - 1]->GetRelativeLocation();
		SnakeBody[BodyElement]->SetRelativeLocation(Location);
	}

	FVector StartPoint = SnakeBody[0]->GetRelativeLocation();
	if (LastMoveDirection == EMovementDirection::UP) { StartPoint.X += SnakeStep; }
	else if (LastMoveDirection == EMovementDirection::DOWN) { StartPoint.X -= SnakeStep; }
	else if (LastMoveDirection == EMovementDirection::LEFT) { StartPoint.Y += SnakeStep; }
	else if (LastMoveDirection == EMovementDirection::RIGHT) { StartPoint.Y -= SnakeStep; }
	//if (MoveDirection.X > 0) StartPoint.X -= SnakeStep;
	//if (MoveDirection.X < 0) StartPoint.X += SnakeStep;
	//if (MoveDirection.Y > 0) StartPoint.Y -= SnakeStep;
	//if (MoveDirection.Y < 0) StartPoint.Y += SnakeStep;
	SnakeBody[0]->SetRelativeLocation(StartPoint);
}

void AMySnakeActor::TakeDamage()
{
	MyPawn->SnakeDestroy();
}

void AMySnakeActor::EatingFood()
{
	MyPawn->DecreaseFood();
}

void AMySnakeActor::CollideTail()
{
	FVector HeadPosition = SnakeBody[0]->GetRelativeLocation();
	for (int i = 1; i < SnakeBody.Num(); ++i) {
		if (SnakeBody[i]->IsVisible()) {
			if (HeadPosition == SnakeBody[i]->GetRelativeLocation()) {
				TakeDamage();
			}
		}
		else {
			break;
		}
	}
}


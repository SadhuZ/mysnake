// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodActor.generated.h"

UCLASS()
class MYSNAKE_API AFoodActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	class USphereComponent* FoodComponent;
	class UStaticMesh* FoodMesh;
	FVector FoodSize;

	void CollectFood();

	//float StepDelay = 2.0;
	//float BuferTime = 0.0;
};

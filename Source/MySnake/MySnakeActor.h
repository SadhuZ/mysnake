// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MySnakeActor.generated.h"

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class MYSNAKE_API AMySnakeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMySnakeActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	int32 SnakeSize = 100;
	float SnakeStep = 45.;
	TArray<UStaticMeshComponent*> SnakeBody;
	class USphereComponent* MyRootComponent;

	void CreateSnakeBody();

	UPROPERTY(EditAnywhere)
		int32 VisibleBodyElements = 4;
	void SetVisibleBodyElements();

	UPROPERTY()
		EMovementDirection LastMoveDirection;
	//UPROPERTY(EditAnywhere)
	//	FVector2D MoveDirection;
	float MovementTimeInterval;
	void MoveSnake();

	int32 Score = 0;

	class AMyPawnCamera* MyPawn;
	void TakeDamage();

	void EatingFood();

	void CollideTail();
};

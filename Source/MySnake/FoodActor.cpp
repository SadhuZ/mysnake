// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodActor.h"
#include "../../../../../../Engine/Source/Runtime/Engine/Classes/Components/SphereComponent.h"
#include "MySnakeActor.h"

// Sets default values
AFoodActor::AFoodActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FoodComponent = CreateDefaultSubobject<USphereComponent>("FoodComponent");
	RootComponent = FoodComponent;

	FoodMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;

	class UMaterialInstance* FoodMaterial;
	FoodMaterial = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(
		TEXT("/Game/SnakeContent/Materials/InstanceMaterials/M_Metal_Gold_Inst.M_Metal_Gold_Inst")).Get();

	FoodSize = FVector(0.5, 0.5, 0.5);
	class UStaticMeshComponent* Food = CreateDefaultSubobject<UStaticMeshComponent>("Food");
	Food->SetStaticMesh(FoodMesh);
	Food->SetRelativeLocation(FVector(0, 0, 0));
	Food->SetRelativeScale3D(FoodSize);
	Food->SetMaterial(0, FoodMaterial);
	Food->AttachTo(FoodComponent);
	Food->SetSimulatePhysics(true);
}

// Called when the game starts or when spawned
void AFoodActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CollectFood();
}

void AFoodActor::CollectFood()
{
	TArray<AActor*> CollectedActors;
	GetOverlappingActors(CollectedActors);

	for (auto pActor : CollectedActors) {
		AMySnakeActor* const Snake = Cast<AMySnakeActor> (pActor);
		if (Snake) {
			Snake->VisibleBodyElements++;
			Snake->Score++;
			Snake->EatingFood();
			Destroy(true, true);
			break;
		}
	}
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyPawnCamera.generated.h"


UENUM() 
enum class EMyGameMode {
	STOP,
	RUN,
	PAUSE
};

UCLASS()
class MYSNAKE_API AMyPawnCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPawnCamera();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// �������� ���� ��� ������ ������
	UPROPERTY(EditAnywhere)
	class UBoxComponent* MyRootComponent;
	// ������ ������
	UPROPERTY(EditAnywhere)
	class USpringArmComponent* CameraSpring;
	// ������ ���������
	UPROPERTY(EditAnywhere)
	class UCameraComponent* MyCamera;

	class AMySnakeActor* MySnakePlayer;
	UFUNCTION(BlueprintCallable, Category = "WarmPawn")
	void CreateSnakePlayer();

	//void FMove(float ButtonVal);
	FVector2D Direction;
	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorisontalInput(float value);
	UFUNCTION()
		void HandlePlayerModeInput(float value);

	float FieldSize = 500.f;
	float SpawnZ = 100.f;
	int32 FoodCount;
	void IncreaseFood() { FoodCount++; }
	void DecreaseFood() { if (FoodCount > 0) FoodCount--; }
	void CreateFood();

	EMyGameMode GameMode;
	EMyGameMode GetMyGameMode() const { return GameMode; }	

	UFUNCTION(BlueprintCallable, Category = "WarmPawn")
		int32 GetScore();

	UFUNCTION(BlueprintCallable, Category = "WarmPawn")
		bool GetGameRun() const { return GameMode == EMyGameMode::RUN; }
	UFUNCTION(BlueprintCallable, Category = "WarmPawn")
		bool GetGamePause() const { return GameMode == EMyGameMode::PAUSE; }
	UFUNCTION(BlueprintCallable, Category = "WarmPawn")
		bool GetGameStop() const { return GameMode == EMyGameMode::STOP; }

	void SnakeDestroy();
};

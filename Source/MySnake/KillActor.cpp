// Fill out your copyright notice in the Description page of Project Settings.


#include "KillActor.h"
#include "../../../../../../Engine/Source/Runtime/Engine/Classes/Components/BoxComponent.h"
#include "MySnakeActor.h"

// Sets default values
AKillActor::AKillActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	class UStaticMesh* WallMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;
	WallColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(
		TEXT("/Game/SnakeContent/Materials/InstanceMaterials/KillMaterial_Inst.KillMaterial_Inst")).Get();

	MyRootComponent = CreateDefaultSubobject<UBoxComponent>("WallModel");
	RootComponent = MyRootComponent;

	class UStaticMeshComponent* WallComponent;
	WallComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall"));
	WallComponent->SetStaticMesh(WallMesh);
	WallComponent->SetMaterial(0, WallColor);
	WallComponent->SetRelativeLocation(FVector(0, 0, 0));
	WallComponent->AttachTo(MyRootComponent);
}

// Called when the game starts or when spawned
void AKillActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKillActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CollideWall();
}

void AKillActor::CollideWall()
{
	TArray<AActor*> CollectedActors;
	GetOverlappingActors(CollectedActors);

	for (auto pActor : CollectedActors) {
		AMySnakeActor* const Snake = Cast<AMySnakeActor>(pActor);
		if (Snake) {
			Snake->TakeDamage();
		}
		else {
			pActor->Destroy(true, true);
		}		
	}
}

